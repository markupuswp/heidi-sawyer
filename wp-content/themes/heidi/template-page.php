<?php
/**
 * Template Name: Intro
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package heidi
 */

get_header(); ?>

<!--<script type="text/javascript" src="https://heidisawyer.kartra.com/page/embed/SLWO16hk2JZa"></script>-->

<?php if( have_rows('content') ): ?>
    <?php while ( have_rows('content') ) : the_row(); ?>

        <?php if( get_row_layout() == 'intro' ): ?>
            <?php get_template_part('template-parts/section/intro'); ?>

        <?php elseif( get_row_layout() == 'preview' ): ?>
            <?php get_template_part('template-parts/section/preview'); ?>

        <?php elseif( get_row_layout() == 'button' ): ?>
            <?php get_template_part('template-parts/section/button'); ?>

        <?php elseif( get_row_layout() == 'steps' ): ?>
            <?php get_template_part('template-parts/section/steps'); ?>

        <?php elseif( get_row_layout() == 'guarantee' ): ?>
            <?php get_template_part('template-parts/section/guarantee'); ?>

        <?php elseif( get_row_layout() == 'progress' ): ?>
            <?php get_template_part('template-parts/section/progress'); ?>

        <?php elseif( get_row_layout() == 'questions' ): ?>
            <?php get_template_part('template-parts/section/questions'); ?>

        <?php elseif( get_row_layout() == 'video_testimonials' ): ?>
            <?php get_template_part('template-parts/section/video-testimonials'); ?>

		<?php elseif( get_row_layout() == 'banner' ): ?>
			<?php get_template_part('template-parts/section/banner'); ?>

		<?php elseif( get_row_layout() == 'grid' ): ?>
			<?php get_template_part('template-parts/section/grid'); ?>

		<?php elseif( get_row_layout() == 'testimonials' ): ?>
			<?php get_template_part('template-parts/section/testimonials'); ?>

		<?php elseif( get_row_layout() == 'social_networks' ): ?>
			<?php get_template_part('template-parts/section/social-networks'); ?>

        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
