<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package heidi
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-9 site-branding">
		            <?php $header_logo = get_field('header_logo', 'options'); ?>
		            <?php if ($header_logo): ?>
                        <img class="header-logo" src="<?php echo $header_logo; ?>" alt="Logo">
		            <?php endif; ?>
                </div>

                <nav id="site-navigation" class="col-md-3 main-navigation">
		            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
                </nav>
            </div>
        </div>
	</header>

	<div id="content" class="site-content">
