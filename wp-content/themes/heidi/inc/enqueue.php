<?php

/**
 * Enqueue scripts and styles.
 */
function heidi_scripts() {

    wp_enqueue_style( 'heidi-style-sidebar', get_template_directory_uri() . '/assets/css/layouts/content-sidebar.css');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/fontawesome/css/font-awesome.min.css');
    wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
	wp_enqueue_style( 'heidi-style', get_stylesheet_uri() );
    //wp_enqueue_style('main-style', get_template_directory_uri() . '/assets/css/style.css');

    wp_enqueue_script( 'heidi-masonry', get_template_directory_uri() . '/assets/js/mymasonry.js', array('masonry'), '20120206', true );
    wp_enqueue_script( 'heidi-calendar', get_template_directory_uri() . '/assets/js/calendar.js', array('jquery'), '20120206', true );
    wp_enqueue_script( 'heidi-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20120206', true );
    wp_enqueue_script( 'heidi-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20130115', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'heidi_scripts' );