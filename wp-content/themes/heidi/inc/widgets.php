<?php

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function heidi_widgets_init() {

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'heidi' ),
        'id'            => 'sidebar-1',
        'description'   => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget 1', 'heidi' ),
		'id'            => 'footer-widget-1',
		'description'   => '',
		'before_widget' => '<div class="col-md-6"><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside></div>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget 2', 'heidi' ),
		'id'            => 'footer-widget-2',
		'description'   => '',
		'before_widget' => '<div class="col-md-6"><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside></div>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

}
add_action( 'widgets_init', 'heidi_widgets_init' );