<?php

/**
 * heidi functions and definitions
 *
 * @package heidi
 */
if ( ! function_exists( 'heidi_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function heidi_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on heidi, use a find and replace
         * to change 'heidi' to the name of your theme in all the template files
         */
        load_theme_textdomain( 'heidi', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'primary' => esc_html__( 'Primary Menu', 'heidi' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        /*
         * Enable support for Post Formats.
         * See http://codex.wordpress.org/Post_Formats
         */
        add_theme_support( 'post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
        ) );

        // Set up the WordPress core custom background feature.
        add_theme_support( 'custom-background', apply_filters( 'heidi_custom_background_args', array(
            'default-color' => 'e2e2e2',
            'default-image' => '',
        ) ) );

        // Add a custom stylesheet to the editor
        add_editor_style();
    }
endif; // heidi_setup
add_action( 'after_setup_theme', 'heidi_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function heidi_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'heidi_content_width', 640 );
}
add_action( 'after_setup_theme', 'heidi_content_width', 0 );

function register_acf_options_pages() {

	if( !function_exists('acf_add_options_page') )
		return;

	$option_page = acf_add_options_page(array(
		'page_title'    => __('Theme settings'),
		'menu_title'    => __('Theme settings'),
		'menu_slug'     => 'theme-settings',
		'capability'    => 'edit_posts',
		'redirect'      => false
	));
}
add_action('acf/init', 'register_acf_options_pages');