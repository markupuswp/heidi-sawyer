<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package heidi
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('footer-widget-1'); ?>
        <?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('footer-widget-2'); ?>
    </footer>
</div><!-- #page -->

<?php wp_footer(); ?>

<?php get_template_part('/template-parts/popup'); ?>

</body>
</html>
