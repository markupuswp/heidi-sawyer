<?php if ( have_rows('popups', 'options') ): ?>
	<div class="popup-content">
	    <?php while( have_rows('popups', 'options') ): the_row(); ?>
	        <?php
			$display = get_sub_field('display');
			$name = get_sub_field('name');
			$content = get_sub_field('content');
		    ?>
	        <?php if ($display && $content): ?>
			    <div id="popup-<?php echo strtolower($name); ?>">
				    <?php echo $content; ?>
			    </div>
	        <?php endif; ?>
	    <?php endwhile; ?>
	</div>
<?php endif; ?>