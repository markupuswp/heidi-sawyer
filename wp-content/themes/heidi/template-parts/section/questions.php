<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$questions = get_sub_field('questions');
$questions_count = count((array)$questions);
?>
<section class="section-questions">
	<?php if ($title): ?>
		<p class="title"><?php echo $title; ?></p>
	<?php endif; ?>
	<?php if ($description): ?>
	    <div class="description"><?php echo $description; ?></div>
	<?php endif; ?>
	<?php if ($title || $description): ?>
		<span class="divider"></span>
	<?php endif; ?>

	<?php if ( have_rows('questions') ): ?>
		<div class="questions-list">
			<div class="col-md-6">
				<?php while( have_rows('questions') ): the_row(); ?>
					<?php
					$counter += 1;
					$question = get_sub_field('question');
					$answer = get_sub_field('answer');
					?>
					<div class="item-<?php echo $counter; ?>">
						<?php if ($question): ?>
							<div class="question"><?php echo $question; ?></div>
						<?php endif; ?>
						<?php if ($answer): ?>
							<div class="answer"><?php echo $answer; ?></div>
						<?php endif; ?>
					</div>

					<?php if ( (int)ceil($questions_count / 2) === (int)$counter ): ?>
						</div><div class="col-md-6">
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
</section>