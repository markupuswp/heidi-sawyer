<?php
$text = get_sub_field('text');
$image = get_sub_field('image');
?>
<section class="section-guarantee">
	<?php if ($text): ?>
        <div class="content">
            <?php echo $text; ?>
        </div>
	<?php endif; ?>

    <?php if ($image): ?>
        <img src="<?php echo $image; ?>" alt="">
    <?php endif; ?>
</section>
