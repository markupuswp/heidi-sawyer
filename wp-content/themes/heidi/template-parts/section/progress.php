<?php
$title = get_sub_field('title');
?>
<section class="section-progress">
	<?php if ($title): ?>
		<div class="section-title">
			<span class="divider"></span>
			<p class="title"><?php echo $title; ?></p>
			<span class="divider"></span>
		</div>
	<?php endif; ?>

    <?php if ( have_rows('description_group') ): ?>
        <div class="description">
	        <?php while( have_rows('description_group') ): the_row(); ?>
                <?php
                $text = get_sub_field('text');
                $image = get_sub_field('image');
                ?>
                <div class="left">
                    <?php if ($text): ?>
                        <?php echo $text; ?>
                    <?php endif; ?>
                </div>
                <div class="right">
                    <?php if ($image): ?>
                        <img src="<?php echo $image; ?>" alt="Progress-image">
                    <?php endif; ?>
                </div>
	        <?php endwhile; ?>
        </div>
    <?php endif; ?>

    <?php if ( have_rows('progress') ): ?>
        <div class="progress-list">
	        <?php while( have_rows('progress') ): the_row();
                $title = get_sub_field('title');
                ?>
                <div class="component">
	                <?php if ($title): ?>
                        <div class="head"><?php echo $title; ?></div>
	                <?php endif; ?>

                    <?php if ( have_rows('items') ): ?>
                        <div class="items">
                            <?php while( have_rows('items') ): the_row();
                            $title = get_sub_field('title');
                            $type = get_sub_field('type');
                            $value = ( $type === 'text' ) ? get_sub_field('value') : "<span class='".$type."'></span>";
                            ?>
                                <div class="value"><?php echo $value; ?></div>
                                <div class="title"><?php echo $title; ?></div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
	        <?php endwhile; ?>
        </div>
    <?php endif; ?>
</section>
