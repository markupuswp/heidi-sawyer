<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$video_group = get_sub_field('video_group');
?>
<section class="section-preview">
    <div class="left">
        <?php if ($video_group['title']): ?>
            <p class="video-title"><?php echo $video_group['title']; ?></p>
        <?php endif; ?>
        <?php if ($title): ?>
            <p class="title"><?php echo $title; ?></p>
        <?php endif; ?>
        <?php if ($description): ?>
            <p class="description"><?php echo $description; ?></p>
        <?php endif; ?>
        <?php if ($video_group['length']): ?>
            <p class="description"><?php echo $video_group['length']; ?></p>
        <?php endif; ?>
    </div>
    <div class="right">
        <?php if ($video_group['video']): ?>
            <?php echo $video_group['video']; ?>
        <?php endif; ?>
    </div>
</section>
