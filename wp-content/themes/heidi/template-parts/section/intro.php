<?php
$title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$headline = get_sub_field('headline');
$background = get_sub_field('background');
$video_group = get_sub_field('video_group');
$buy_group = get_sub_field('buy_group');
?>
<section class="section-intro">
	<div class="top" <?php if( $background ) echo "style='background: url(" . $background . ")'"; ?>>
        <div class="container">
	        <?php if ($title): ?>
                <p class="title"><?php echo $title; ?></p>
	        <?php endif; ?>
	        <?php if ($subtitle): ?>
                <div class="subtitles"><?php echo $subtitle; ?></div>
	        <?php endif; ?>
	        <?php if ($headline): ?>
                <p class="headline"><?php echo $headline; ?></p>
	        <?php endif; ?>
        </div>
	</div>

	<div class="body">
		<div class="left">
			<?php if ( have_rows('video_group') ): ?>
				<?php while( have_rows('video_group') ): the_row(); ?>
					<?php
					$title = get_sub_field('title');
					$description = get_sub_field('description');
					$video = get_sub_field('video');
					?>
					<?php if ($title): ?>
						<p class="title"><?php echo $title; ?></p>
					<?php endif; ?>
					<?php if ($video): ?>
						<video src="<?php echo $video; ?>"></video>
					<?php endif; ?>
					<?php if ($description): ?>
						<p class="description"><?php echo $description; ?></p>
					<?php endif; ?>

				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="right">
			<?php if ( have_rows('buy_group') ): ?>
				<?php while( have_rows('buy_group') ): the_row(); ?>
					<?php
					$icon = get_sub_field('icon');
					$title = get_sub_field('title');
					$subtitle = get_sub_field('subtitle');
					$description = get_sub_field('description');
					$button = get_sub_field('button');
					?>
					<?php if ($icon): ?>
						<img src="<?php echo $icon; ?>" alt="">
					<?php endif; ?>
					<?php if ($title): ?>
						<p class="title"><?php echo $title; ?></p>
					<?php endif; ?>
					<?php if ($subtitle): ?>
						<p class="subtitle"><?php echo $subtitle; ?></p>
					<?php endif; ?>
					<?php if ($description): ?>
						<p class="description"><?php echo $description; ?></p>
					<?php endif; ?>
					<?php if ($button): ?>
						<a href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>