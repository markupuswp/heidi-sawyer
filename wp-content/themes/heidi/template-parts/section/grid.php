<section class="section-grid">
	<?php if ( have_rows('items') ): ?>
	    <?php while( have_rows('items') ): the_row(); ?>
			<?php
			$type = get_sub_field('type');
			$image = get_sub_field('image');
			$video = get_sub_field('video');
			?>
			<div class="grid-item col-md-4">
				<?php if ($image): ?>
					<img src="<?php echo $image; ?>" alt="grid-image">
				<?php endif; ?>
				<?php if ($video): ?>
					<?php echo $video; ?>
				<?php endif; ?>
			</div>
	    <?php endwhile; ?>
	<?php endif; ?>
</section>