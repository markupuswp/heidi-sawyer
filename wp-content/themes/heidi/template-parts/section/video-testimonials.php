<?php
$background = get_sub_field('background');
$testimonials = get_sub_field('testimonials');
$testimonials_count = count((array)$testimonials);
?>
<section class="section-video-testimonials" <?php if( $background ) echo "style='background: url(" . $background . ")'"; ?>>
	<?php if ( have_rows('testimonials') ): ?>
        <div class="row">
	        <?php while( have_rows('testimonials') ): the_row(); ?>
		        <?php
		        $counter += 1;
		        $video = get_sub_field('video');
		        $comment = get_sub_field('comment');
		        $image = get_sub_field('image');
		        $name = get_sub_field('name');
		        $profession = get_sub_field('profession');
		        ?>
                <div class="col-md-6">
                    <?php if ($video): ?>
                        <div class="video">
                            <?php echo $video; ?>
                        </div>
                    <?php endif; ?>

                    <div class="content">
                        <?php if ($comment): ?>
                            <div class="comment">"<?php echo trim($comment); ?>".</div>
                        <?php endif; ?>

	                    <?php if ($image): ?>
                            <div class="photo">
                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="">
                            </div>
	                    <?php endif; ?>

	                    <?php if ($name): ?>
                            <div class="name">
			                    <?php echo $name; ?>
                            </div>
	                    <?php endif; ?>

	                    <?php if ($profession): ?>
                            <div class="profession">
			                    <?php echo $profession; ?>
                            </div>
	                    <?php endif; ?>
                    </div>
                </div>

                <?php if ( $counter !== $testimonials_count && $counter % 2 === 0 ): ?>
                    </div><div class="row">
                <?php endif; ?>
	        <?php endwhile; ?>
        </div>
	<?php endif; ?>
</section>