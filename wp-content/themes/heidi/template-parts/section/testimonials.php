<?php
$testimonials = get_sub_field('testimonials');
$testimonials_count = count((array)$testimonials);
?>
<section class="section-testimonials">
	<?php if ( have_rows('testimonials') ): ?>
        <div class="slider">
	        <?php while( have_rows('testimonials') ): the_row(); ?>
		        <?php
		        $counter += 1;
		        $comment = get_sub_field('comment');
		        $image = get_sub_field('image');
		        $name = get_sub_field('name');
		        $profession = get_sub_field('profession');
		        ?>
                <div class="slider-item">
                    <div class="content">
                        <?php if ($comment): ?>
                            <div class="comment">"<?php echo trim($comment); ?>".</div>
                        <?php endif; ?>

	                    <?php if ($image): ?>
                            <div class="photo">
                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="">
                            </div>
	                    <?php endif; ?>

	                    <?php if ($name): ?>
                            <div class="name">
			                    <?php echo $name; ?>
                            </div>
	                    <?php endif; ?>

	                    <?php if ($profession): ?>
                            <div class="profession">
			                    <?php echo $profession; ?>
                            </div>
	                    <?php endif; ?>
                    </div>
                </div>

                <?php if ( $counter !== $testimonials_count && $counter % 2 === 0 ): ?>
                    </div><div class="row">
                <?php endif; ?>
	        <?php endwhile; ?>
        </div>
	<?php endif; ?>
</section>