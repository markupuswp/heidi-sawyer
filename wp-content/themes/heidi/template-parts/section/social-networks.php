<?php $display = get_sub_field('display'); ?>

<?php if ($display): ?>
    <?php $title = get_sub_field('title'); ?>
	<section class="section-social-networks">
		<?php if ($title): ?>
			<p class="title"><?php echo $title; ?></p>
		<?php endif; ?>

		<?php if ( have_rows('social_networks', 'options') ): ?>
			<div class="social-networks">
			    <?php while( have_rows('social_networks', 'options') ): the_row(); ?>
			        <?php
				    $title = get_sub_field('title');
				    $link = get_sub_field('link');
				    ?>
					<div class="social-item">
						<?php if ($link): ?>
							<a href="<?php echo $link; ?>" target="_blank">
								<?php if ($title): ?>
									<span class="social-item-<?php echo strtolower($title); ?>"></span>
									<?php echo $title; ?>
								<?php endif; ?>
							</a>
						<?php endif; ?>
					</div>
			    <?php endwhile; ?>
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>