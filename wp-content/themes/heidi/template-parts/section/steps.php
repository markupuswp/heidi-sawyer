<?php
$title = get_sub_field('title');
?>
<section class="section-steps">
	<?php if ($title): ?>
		<p class="title"><?php echo $title; ?></p>
	<?php endif; ?>

	<?php if ( have_rows('steps') ): ?>
		<div class="steps-wrapper">
			<?php while( have_rows('steps') ): the_row(); ?>
				<?php
				$counter += 1;
				$title = get_sub_field('title');
				$icon = get_sub_field('icon');
				?>
				<div class="col-md-4">
					<?php if ($icon): ?>
						<img src="<?php echo $icon['sizes']['medium']; ?>" alt="Step-<?php echo $counter; ?>">
					<?php endif; ?>

					<?php if ($title): ?>
						<p><?php echo $title; ?></p>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>
