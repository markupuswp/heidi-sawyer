<?php
$template = get_sub_field('template');
$button = get_sub_field('button');
?>
<section class="section-button <?php echo $template; ?>">
    <!--template-1-->
	<?php if ( $template === 'template-1' ) :
		$text = get_sub_field('text');
		$title = get_sub_field('title');
		?>
		<?php if ($text): ?>
			<p><?php echo $text; ?></p>
		<?php endif; ?>
		<?php if ($title): ?>
			<p><?php echo $title; ?></p>
		<?php endif; ?>
		<span class="divider"></span>
		<?php if ($button): ?>
			<a class="btn" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
		<?php endif; ?>
		<span class="divider"></span>

    <!--template-2-->
    <?php elseif ( $template === 'template-2' ): ?>
		<?php if ($button): ?>
            <a class="btn btn-simple" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
		<?php endif; ?>

    <!--template-3-->
	<?php elseif ( $template === 'template-3' ): ?>
		<?php if ($button): ?>
            <a class="btn btn-long" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
		<?php endif; ?>

	<?php endif; ?>
</section>
