<?php
$image = get_sub_field('image');
?>
<section class="section-banner">
	<?php if ($image): ?>
		<img src="<?php echo $image; ?>" alt="banner">
	<?php endif; ?>
</section>